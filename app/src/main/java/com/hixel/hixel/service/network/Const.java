package com.hixel.hixel.service.network;

class Const {
    static final String BASE_URL = "https://game.bones-underground.org:8443";
    static final String NO_AUTHENTICATION = "No-Authentication";
    static final int REQUEST_TIMEOUT = 60;
}